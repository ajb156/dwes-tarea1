
  <?php
    include_once './partials/header.php';
    include_once './partials/nav.php';
  ?>
  
  <main>
    <h1>Desenvolvemento web en contorno servidor.</h1>
    <section class="details">
      <h2>Unidad 1:</h2>
      <h3> En una primera aproximación, tu aplicación se compondrá de tres páginas:</h3>
      <ul>
        <li>Una página de presentación, donde explicas el cometido de la aplicación y su funcionamiento. Contiene un enlace que te lleva a la siguiente página.</li>
        <li>Una página de introducción de datos, donde cualquiera puede darse de alta introduciendo su nombre y su dirección de correo. Contiene un formulario que, una vez rellenado, envía los datos y te lleva a la siguiente página.</li>
        <li>Una página de visualización de datos, en la que se muestra la lista, nombres y direcciones de correo, de todos los que se han anotado en la aplicación.</li>
      </ul>

      <h3>Gestion de correos electronicos:</h3>
      <ul>
        <li>Aplicación que permite registar contactos por su correo electronico.</li>
        <li>Los contactos son guardados en un archivo de texto.</li>
      </ul>
    </section>
  </main>

  <?php
    include_once './partials/footer.php';
  ?>
